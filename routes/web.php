<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ArticleController@index');

Auth::routes();
Route::get('user/block','UserController@create');
Route::post('user/block','UserController@store');
// Route::get('comments/{comment}/replies','ReplyController@index');
// Route::get('articles/tags/{tag}','TagController@index');
Route::get('home/login', 'Auth/LoginController@login');
Route::get('home', 'HomeController@index')->name('home');
Route::get('home/logout', 'HomeController@destroy');
Route::post('home', 'ArticleController@store');
Route::get('articles/create', 'ArticleController@create');
Route::get('articles/{id}', 'ArticleController@show');
Route::get('home/articles/{id}', 'HomeController@show');
Route::post('articles/{article}/comments', 'CommentController@store');

// Route::post('home', 'ArticleController@store');
