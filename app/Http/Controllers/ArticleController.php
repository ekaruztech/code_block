<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lang;
use App\Frame;
use App\Article;
use App\User;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index()
    {
        $articles = Article::all();
        //dd($articles);
        //dd(auth()->user()->role);

        return view('visitor.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // dd(auth()->user()->role);
        if(auth()->user()->role == 1)
        {
            $langs = Lang::all();
            $frames = Frame::all();
            return view('layouts.create',compact('langs','frames'));
        }

        return redirect()->home();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd(request(['frames','langs']));
        $this->validate($request,[
            'title' => 'required',
            'langs' => 'required',
            'frames' => 'required',
            'content' => 'required'
        ]);

        Article::create([
                'title' =>request('title'),
                'lang_id' =>request('langs'),
                'frame_id' =>request('frames'),
                'content' =>request('content'),
                'user_id' =>auth()->user()->id
            ]);

        return redirect()->home();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::findorfail($id);
        //dd($user);
        // dd($article);
        return view('visitor.visitcomm', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
