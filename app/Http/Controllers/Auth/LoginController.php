<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }

    public function login(LoginRequest $request)
    {

        $username = $request->login; //the input field has name='username' in form
        $password = $request->password;
        //dd($username);

        if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
            //user sent their email 
            Auth::attempt(['email' => $username, 'password' => $password, 'role' => 1]);
            Auth::attempt(['email' => $username, 'password' => $password, 'role' => 2]);
        } else {
            //they sent their username instead
            Auth::attempt(['username' => $username, 'password' => $password, 'role'=> 1]); 
            Auth::attempt(['username' => $username, 'password' => $password, 'role'=> 2]);
        }

        //was any of those correct ?
            if ( Auth::check()) {
                //send them where they are going 
                return redirect('home');
            }

            //Nope, something wrong during authentication 
            return redirect()->back()->withErrors([
                'credentials' => 'Please, check your credentials'
            ]);
    }
}
