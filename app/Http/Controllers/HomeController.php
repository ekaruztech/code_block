<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $id = auth()->user()->id;
        $articles = Article::all();//->where('user_id',$id);
        //dd($articles);

        return view('home', compact('articles'));
    }

    public function show($id)
    {
        $article = Article::findorfail($id);
        //$myarticles = Article::all()->where('user_id',$id);
        //dd($myarticles);
        //   dd($article);
        return view('visitor.visitcomm', compact('article'));
    }

    public function destroy()
    {
        auth()->logout();
        return redirect('/');
    }
}

