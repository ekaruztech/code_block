<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lang extends Model
{
    protected $fillable = [
        'languages',
    ];

    public function frames(){
        return $this->hasMany(Frame::class);
    }
}
