<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Frame extends Model
{
    protected $fillable = [
        'frameworks', 'lang_id',
    ];

    public function lang(){
       return $this->belongsTo(Lang::class);
    }
}
