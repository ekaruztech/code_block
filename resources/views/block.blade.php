@extends('layouts.app')

@section('content')
  
    <div class="container col-lg-offset-4">
        <form role="form" method="post" action="/user/block">
            {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                          <!-- <label for="user"></label> -->
                          <input type="text" class="form-control" id="user" name="user">
                      </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary ">Block</button>
                            </div>
                        </div>

                        @if(count($errors))
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    @include('errors')
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
        </form>
    </div>
@endsection