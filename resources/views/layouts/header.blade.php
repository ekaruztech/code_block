<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}">&lt;Codeblock/&gt;</a>
        </div>
        
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="#">Questions?</a></li>
                <li><a href="#">Tags</a></li>
                <li><a href="#">Projects</a></li> 
                <li><a href="#">About</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{url('/login')}}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <li class="active"><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
        </div>         
    </div>
</nav>