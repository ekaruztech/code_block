@extends('layouts.app')

@section('articlecreate')
  
    <div class="container col-lg-offset-4">
        <form role="form" method="post" action="/home">
            {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" class="form-control" id="title" name="title">
                      </div>
                        <div class="form-group">
                          <label for="langs">Languages</label>
                              <select class="form-control" id="langs" name="langs">
                                  <option value=0>--Select--</option>
                                  @foreach($langs as $lang)
                                      <option value="{{$lang->id}}">{{$lang->languages}}</option>
                                  @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                            <label for="frames">Frames</label>
                            <select class="form-control" id="frames" name="frames">
                                <option value=0>--Select--</option>
                                @foreach($frames as $frame)
                                  <option value="{{$frame->id}}">{{$frame->frameworks}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea type="text" rows="5" class="form-control" id="content" name="content"></textarea>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-primary">Publish</button>
                            </div>
                        </div>

                        @if(count($errors))
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    @include('errors')
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
        </form>
    </div>
@endsection