<nav class="navbar navbar-default navbar-fixed-top">
<div class="container">
    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">&lt;Codeblock/&gt;</a>
    </div>
    
    <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
            <li><a href="#">My Posts</a></li>
            <li><a href="#">Tags</a></li>
            <li><a href="#">Projects</a></li> 
            <li><a href="#">About</a></li> 
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <span class="glyphicon glyphicon-user"></span> 
                        {{auth()->user()->username}}
                    <span class="caret"></span>
                </a>
                
                <ul class="dropdown-menu">
                    <li><a href="#">Publish</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Edit profile</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Change Password</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Logout</a></li>
                </ul>  
            </li>
        </ul>
    </div>
    
</div>
</nav>