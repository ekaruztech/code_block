@extends('layouts.master')

@section('content')
@if(Auth::check())
    @include('layouts.homeheader')
@else
    @include('layouts.header')
@endif
<div class="container mainbody">
    <div class="row">
        
        <div class="col-lg-6 col-lg-offset-2 " style="background-color:white;">
            <article class="col-lg-12">
           
                <h3>{{$article->title}}</h3>
                 @if(count($article->tags))
                    <ul>
                        @foreach($article->tags as $tag)
                            <li>
                                {{$tag->name}}
                            </li>
                        @endforeach
                    </ul>
                 @endif
                
                <h6 style="color:#DC143C;">Posted by {{$article->user->name}} ({{$article->created_at->diffForHumans()}})</h6>
                <hr>
                <h5>Brief description</h5>
                <div class="well">          
                    <code>
						{{$article->content}}
                    </code>

                    @foreach($article->comments as $comment)
                        <li><small><b>{{$comment->created_at->diffForhumans()}} by {{$comment->user->username}}:</b></small><br>
                        <p style="margin-left:30px;">{{$comment->body}}</p>
                            <ul>
                               @foreach($comment->replies as $reply)  
                                    <li>
                                        <small><b>{{$reply->created_at->diffForhumans()}} by {{$reply->user->username}}:</b>
                                        </small><br><p>{{$reply->body}}</p>
                                    </li>
                               @endforeach
                           </ul>
                        </li>
                    @endforeach
                </div>   
                
                <form role="form" method="POST" action="/articles/{{$article->id}}/comments">
                        {{csrf_field()}}
                    <div class="form-group">
                        <label for="body">Comment:</label>
                        <textarea rows="4" class="form-control" id="body" name="body"></textarea>
                    </div>
                    <button type="submit" class="btn btn-danger btn-sm commbtn">Comment</button>
                </form>
            </article>
            <div class="col-lg-12"></div>
            
        </div>
        @include('layouts.sidebar')

    </div>
</div>
@endsection
