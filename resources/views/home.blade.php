@extends('layouts.app')

@section('content')
    <div class="container mainbody">
        <div class="row">
            
            <div class="col-lg-6 col-lg-offset-2 " style="background-color:white;">
                 @foreach($articles as $article)
                    <article class="col-lg-12">
                        <h3><a  href="{{url('home/articles',$article->id)}}" class="question">{{$article->title}}</a></h3>
                        <h6 style="color:#DC143C;">{{$article->content}}</h6>  
                    </article>
                    <hr class="col-lg-9"> 
                @endforeach
                
            </div>
            @include('layouts.sidebar')

        </div>
    </div>
@endsection
