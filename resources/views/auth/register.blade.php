@extends('layouts.master')

@section('content')
@include('layouts.regheader')

<div class="container regbody">
    <div class="row">
        <div class="col-lg-4 col-xs-8 col-xs-offset-2 col-lg-offset-5">
                                
            <form class="col-lg-10 col-lg-offset-1 regform" role="form" method="POST" action="{{ route('register') }}">
                {{csrf_field()}}

                <div class="form-group">
                  <label for="username">Username:</label>
                  <input type="text" class="form-control input-sm" id="username" name="username" required>
                </div>

                <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text" class="form-control input-sm" id="name" name="name" required>
                </div>
                
                <div class="form-group">
                  <label for="phone">Phone Number:</label>
                  <input type="number" class="form-control input-sm" id="phone" name="phone" required>
                </div>
                
                <div class="form-group">
                  <label for="email">Email address:</label>
                  <input type="email" class="form-control input-sm" id="email" name="email" required>
                </div>
    
                <div class="form-group">
                  <label for="pwd">Password:</label>
                  <input type="password" class="form-control input-sm" id="pwd" name="password" required>
                </div>

                <div class="form-group">
                  <label for="cpwd">Confirm Password:</label>
                  <input type="password" class="form-control input-sm" id="cpwd" name="password_confirmation">
                </div>

                <!-- <div class="checkbox">
                  <label><input type="checkbox"> Remember me</label>
                </div> -->
                <button type="submit" class="btn btn-danger btn-sm regbtn">Sign Up</button>
                @include('errors')
            </form>
        </div>
    </div>
</div>>
 @endsection