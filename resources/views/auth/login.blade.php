
@extends('layouts.master')

@section('content')
@include('layouts.loginheader')
<div class="container loginbody">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-5">
                                
            <form class="col-lg-10 col-xs-10 col-xs-offset-1 col-lg-offset-1 loginform" role="form" method="POST" action="{{ route('login') }}">
                {{csrf_field()}}

                <div class="form-group input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login" type="text" class="form-control" name="login"  required placeholder="username/email"><br>
                </div>
                <div class="form-group  input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                </div>
                 @include('errors')

                <div class="form-group">
                    <div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-danger btn-sm loginbtn">Login</button>
            </form>
        </div>
    </div>
</div>
@endsection
